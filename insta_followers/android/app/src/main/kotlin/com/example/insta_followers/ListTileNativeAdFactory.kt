package com.example.insta_followers

import android.content.Context
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.google.android.gms.ads.MediaContent
import com.google.android.gms.ads.formats.NativeAd
import com.google.android.gms.ads.formats.UnifiedNativeAd
import com.google.android.gms.ads.formats.UnifiedNativeAdView
import com.google.android.gms.ads.nativead.MediaView
import io.flutter.plugins.googlemobileads.GoogleMobileAdsPlugin


internal class ListTileNativeAdFactory(private val context: Context) : GoogleMobileAdsPlugin.NativeAdFactory {
    override fun createNativeAd(
            nativeAd: UnifiedNativeAd, customOptions: Map<String?, Any?>?): UnifiedNativeAdView {
        val nativeAdView: UnifiedNativeAdView = LayoutInflater.from(context)
                .inflate(R.layout.list_tile_native_ad, null) as UnifiedNativeAdView
        nativeAdView.setNativeAd(nativeAd)
        val attributionViewSmall: TextView = nativeAdView
                .findViewById(R.id.tv_list_tile_native_ad_attribution_small)
        val attributionViewLarge: TextView = nativeAdView
                .findViewById(R.id.tv_list_tile_native_ad_attribution_large)

        val imageView: ImageView = nativeAdView.findViewById(R.id.iv_list_tile_native_ad_image)
        val image: List<NativeAd.Image> = nativeAd.images
        if (nativeAd.images != null) {
            imageView.setImageDrawable(image[0].drawable)
        } else {
            imageView.setBackgroundColor(Color.GRAY)
        }

        nativeAdView.iconView = nativeAdView.findViewById(R.id.iv_list_tile_native_ad_icon)
        if (nativeAd.icon == null) {
            nativeAdView.iconView.visibility = View.GONE
            attributionViewSmall.visibility = View.INVISIBLE
            attributionViewLarge.visibility = View.VISIBLE
        } else {
            (nativeAdView.iconView as ImageView).setImageDrawable(
                    nativeAd.icon.drawable
            )
            nativeAdView.iconView.visibility = View.VISIBLE
            attributionViewSmall.visibility = View.VISIBLE
            attributionViewLarge.visibility = View.INVISIBLE
        }

        val headlineView: TextView = nativeAdView.findViewById(R.id.tv_list_tile_native_ad_headline)
        headlineView.text = nativeAd.headline
        nativeAdView.headlineView = headlineView
        val bodyView: TextView = nativeAdView.findViewById(R.id.tv_list_tile_native_ad_body)
        bodyView.text = nativeAd.body
        bodyView.visibility = if (nativeAd.body != null) View.VISIBLE else View.INVISIBLE
        nativeAdView.bodyView = bodyView

        nativeAdView.callToActionView = nativeAdView.findViewById(R.id.tv_list_tile_native_ad_callToActionButton)
        if (nativeAd.callToAction == null) {
            nativeAdView.callToActionView.visibility = View.INVISIBLE
        } else {
            nativeAdView.callToActionView.visibility = View.VISIBLE
            (nativeAdView.callToActionView as Button).text = nativeAd.callToAction
        }

        return nativeAdView
    }
}