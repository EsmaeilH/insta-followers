import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_analytics/observer.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'src/ui/screens/splashScreen.dart';

class App extends StatelessWidget {

  // This widget is the root of your application.
  firebaseInit() async{
    await Firebase.initializeApp();
  }

  @override
  Widget build(BuildContext context) {
    const PrimaryColor = const Color(0xFF151026);
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);

    firebaseInit();
    FirebaseAnalytics analytics = FirebaseAnalytics();
    return MaterialApp(
      // debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: PrimaryColor
      ),
      home: SplashScreen(),
      navigatorObservers: [
        FirebaseAnalyticsObserver(analytics: analytics),
      ],
    );
  }
}
