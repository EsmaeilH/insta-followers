import 'package:debug_mode/debug_mode.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';

class AdState {
  Future<InitializationStatus> initialization;

  AdState(this.initialization);

  String get bannerAdUnitId => DebugMode.isInDebugMode
      ? 'ca-app-pub-3940256099942544/6300978111'
      : 'ca-app-pub-1028980534156188/9999504715';

  String get interstitialLoginAdUnitId => DebugMode.isInDebugMode
      ? 'ca-app-pub-3940256099942544/1033173712'
      : 'ca-app-pub-1028980534156188/5972629795';

  String get interstitialPostAdUnitId => DebugMode.isInDebugMode
      ? 'ca-app-pub-3940256099942544/1033173712'
      : 'ca-app-pub-1028980534156188/8478166720';

  String get interstitialUnfollowAdUnitId => DebugMode.isInDebugMode
      ? 'ca-app-pub-3940256099942544/1033173712'
      : 'ca-app-pub-1028980534156188/9024961633';

  String get interstitialFollowerAdUnitId => DebugMode.isInDebugMode
      ? 'ca-app-pub-3940256099942544/1033173712'
      : 'ca-app-pub-1028980534156188/6426718452';

  String get rewardedUnfollowAdUnit => DebugMode.isInDebugMode
      ? 'ca-app-pub-3940256099942544/5224354917'
      : 'ca-app-pub-1028980534156188/1920885907';

  String get rewardedLotteryAdUnit => DebugMode.isInDebugMode
      ? 'ca-app-pub-3940256099942544/5224354917'
      : 'ca-app-pub-1028980534156188/7937264768';

  String get nativePostListAdUnit => DebugMode.isInDebugMode
      ? 'ca-app-pub-3940256099942544/8135179316'
      : 'ca-app-pub-1028980534156188/4571792877';

}