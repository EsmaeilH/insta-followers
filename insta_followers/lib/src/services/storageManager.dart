import 'dart:async';
import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:insta_followers/src/model/postModel.dart';
import 'package:insta_followers/src/model/userModel.dart';
import 'package:shared_preferences/shared_preferences.dart';

class StorageManager {
  static final StorageManager _storageManager = StorageManager._internal();

  factory StorageManager() {
    return _storageManager;
  }

  StorageManager._internal();

  var _dio = Dio();

  String _latestVersion;
  String _followerQueryHash;
  String _followingQueryHash;
  String _forceUpdate;
  String _instaCookie;
  String _webAppId;
  String _webUserAgent;

  String get latestVersion => _latestVersion;
  String get followerQueryHash => _followerQueryHash;
  String get followingQueryHash => _followingQueryHash;
  String get forceUpdate => _forceUpdate;
  String get instaCookie => _instaCookie;
  String get webAppId => _webAppId;
  String get webUserAgent => _webUserAgent;


  Future<void> getSettingFromServer() async{
    String settingUrl = 'https://i.tumantra.com/api/insta-follower/v1/settings';
      Response response = await _dio.get(settingUrl);
      for (int i=0 ; i < ((jsonDecode(response.toString())['data']) as List).length ; i++ ) {
        switch (jsonDecode(response.toString())['data'][i]['name']) {
          case 'currentVersion':
            _latestVersion = jsonDecode(response.toString())['data'][i]['value'];
            break;
          case 'followerQueryHash':
            _followerQueryHash = jsonDecode(response.toString())['data'][i]['value'];
            break;
          case 'followingQueryHash':
            _followingQueryHash = jsonDecode(response.toString())['data'][i]['value'];
            break;
          case 'forceUpdate':
            _forceUpdate = jsonDecode(response.toString())['data'][i]['value'];
            break;
          case 'instaCookie':
            _instaCookie = jsonDecode(response.toString())['data'][i]['value'];
            break;
          case 'webAppId':
            _webAppId = jsonDecode(response.toString())['data'][i]['value'];
            break;
          case 'webUserAgent':
            _webUserAgent = jsonDecode(response.toString())['data'][i]['value'];
            break;

          default:
            break;
        }
      }
  }

  void saveUpdateDataDB(String _instaID,String _imgUrl ,int _followers ,int _following, String _description, bool _update) async{
    String saveUrl = 'https://i.tumantra.com/api/insta-follower/v1/user?hl=en';
    var user = {
      "username": _instaID,
      "description": _description,
      "followers": _followers,
      "following": _following,
      "image_url": _imgUrl,
      "update": _update
    };
    try{
      Response response = await _dio.post(saveUrl, data: user);
      print('this is post status code: ${response.statusCode}');
    }on DioError catch(err){
      print('save and update err code: ${err.response.statusCode}');
    }
  }

  Future<UserModel> getDataDB() async{
    String getUrl = 'https://i.tumantra.com/api/insta-follower/v1/user?hl=en';

    try{
      Response response = await _dio.get(getUrl);
      return UserModel.fromJson(response.data);
    }on DioError catch(err){
      print('get data err code: ${err.response.statusCode}');
      throw Exception('Failed to load users');
    }
  }

  Future<Response> addUserToLotteryList(String _instaID) async {
    String userVideoSeenUrl = 'https://i.tumantra.com/api/insta-follower/v1/video?hl=en';

    var user = {
      "username": _instaID,
    };

    Response response = await _dio.post(userVideoSeenUrl, data: user);
    return response;
  }

  Future<Response> addPostToLotteryList(String _postID) async {
    String postVideoSeenUrl = 'https://i.tumantra.com/api/insta-follower/v1/post-video?hl=en';

    var post = {
      "id": _postID,
    };

    Response response = await _dio.post(postVideoSeenUrl, data: post);
    return response;
  }

  Future<Response> saveUserPost(String _thumbnailUrl, String _postId, String _authorName) async{
    String postToServerUrl = 'https://i.tumantra.com/api/insta-follower/v1/post?hl=en';
    var post = {
      "thumbnail_url": _thumbnailUrl,
      "id": _postId,
      "username": _authorName
    };
    try {
      Response response = await _dio.post(postToServerUrl, data: post);
      return response;
    } on DioError catch(err) {
      print("Error happened in saveUserPost: ${err.response}");
      throw Exception(err.response);
    }
  }

  Future<PostModel> getUsersPost() async{
    String getUsersPostFromServerUrl = 'https://i.tumantra.com/api/insta-follower/v1/post?hl=en';

    try {
      Response response = await _dio.get(getUsersPostFromServerUrl);
      return PostModel.fromJson(response.data);
    } on DioError catch(err) {
      print("Error happened in saveUserPost: ${err.response.statusCode}");
      throw Exception(err.response.statusMessage);
    }
  }

  void saveDataPref(String key, dynamic value) async {
    final prefs = await SharedPreferences.getInstance();
    if (value is int) {
      prefs.setInt(key, value);
    } else if (value is String) {
      prefs.setString(key, value);
    } else if (value is bool) {
      prefs.setBool(key, value);
    } else {
      print("Invalid Type");
    }
  }

  Future<dynamic> readDataPref(String key) async {
    final prefs = await SharedPreferences.getInstance();
    dynamic obj = prefs.get(key);
    return obj;
  }

}

final storageInstance = StorageManager();