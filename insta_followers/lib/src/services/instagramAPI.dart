
import 'package:dio/dio.dart';
import 'package:insta_followers/src/model/nodeModel.dart';
import 'globalVariable.dart';
import 'storageManager.dart';

class PrepareJson{
  var dio = Dio();

  Future<Response> getUserProfileJson(String id, String _cookie) async{
    dio.interceptors.add(InterceptorsWrapper(onRequest: (RequestOptions options) {
      var customHeaders = {
        'cookie': _cookie
        // other headers
      };
      options.headers.addAll(customHeaders);
      return options;
    }));
    try{
      Response response = await dio.get("https://www.instagram.com/$id/?__a=1");
      return response;
    }on DioError catch(err){
      if(err.response.statusCode == 404){
        throw Exception('Invalid username');
      }else{
        throw Exception('error happened in fetching data');
      }
    }
  }

  Future<Response> checkLoginUsername() async{
    var url = 'https://www.instagram.com/accounts/edit/?__a=1';
    dio.interceptors.add(InterceptorsWrapper(onRequest: (RequestOptions options) {
      var customHeaders = {
        'cookie': cookie
        // other headers
      };
      options.headers.addAll(customHeaders);
      options.followRedirects = false;
      return options;
    }));
    try{
      Response response = await dio.get(url);
      return response;
    }on DioError catch(err){
      if(err.response.statusCode == 302){
        throw Exception('You are not login!');
      }
      return Response();
    }
  }

  Future<Edges> getFollowerJson(String type, String _cookie, String _endCursor) async{
    String _id = await storageInstance.readDataPref('id');
    String _hash =(type == 'follower')? await storageInstance.readDataPref('followerQueryHash')
        : await storageInstance.readDataPref('followingQueryHash');
    String requestUrl =(_endCursor == null)? 'https://www.instagram.com/graphql/query/?query_hash=$_hash&variables='+
        Uri.encodeComponent('{"id":"$_id","include_reel":true,"fetch_mutual":true,"first":50}')
        :
        'https://www.instagram.com/graphql/query/?query_hash=$_hash&variables='+
        Uri.encodeComponent('{"id":"$_id","include_reel":true,"fetch_mutual":true,"first":50,"after":"$_endCursor"}');
    dio.interceptors.add(InterceptorsWrapper(onRequest: (RequestOptions options) {
      var customHeaders = {
        'cookie': _cookie
        // other headers
      };
      options.headers.addAll(customHeaders);
      return options;
    }));
    try{
      Response response = await dio.get(requestUrl);
      return type == 'follower' ?
      Edges.fromJson(response.data['data']['user']['edge_followed_by'])
          :
      Edges.fromJson(response.data['data']['user']['edge_follow']);

    }on DioError catch(err){
      if(err.response.statusCode == 400){
        print(err.response.statusCode);
      }else if(err.response.statusCode == 404) {
        print(err.response.statusCode);
      }else{
        print(err.response.statusCode);
      }
      return Edges();
    }
  }

  Future<Response> unfollowRequest(String _id) async{
    String url = 'https://www.instagram.com/web/friendships/$_id/unfollow/';
    dio.interceptors.add(InterceptorsWrapper(onRequest: (RequestOptions options) {
      var customHeaders = {
        'x-csrftoken': csrftoken,
        'cookie': cookie
        // other headers
      };
      options.headers.addAll(customHeaders);
      options.followRedirects = false;
      return options;
    }));
    try{
      Response response = await dio.post(url);
      return response;
    }on DioError catch(err){
      if(err.response.statusCode == 302){
        throw Exception('You are not login!');
      }else {
        throw Exception('Error happened. try again');
      }
    }
  }

  Future<Response> logOutRequest() async{
    String url = 'https://www.instagram.com/accounts/logout/ajax/';
    dio.interceptors.add(InterceptorsWrapper(onRequest: (RequestOptions options) {
      var customHeaders = {
        'x-csrftoken': csrftoken,
        'cookie': cookie
        // other headers
      };
      options.headers.addAll(customHeaders);
      return options;
    }));
    try{
      Response response = await dio.post(url);
      print(response.statusCode);
      return response;
    }on DioError catch(err){
      print('logout err message: ${err.response.statusMessage}');
      return Response();
    }
  }

  Future<Response> generatePostEmbedLink(String _postId, String _userAgent, String _appID) async {
    String url = 'https://api.instagram.com/oembed/?url=https://www.instagram.com/p/$_postId&hidecaption=0&maxwidth=540';

    dio.interceptors.add(InterceptorsWrapper(onRequest: (RequestOptions options) {
      var customHeaders = {
        'user-agent': _userAgent,
        'x-ig-app-id': _appID
      };
      options.headers.addAll(customHeaders);
      return options;
    }));
    Response response = await dio.get(url);
    return response;
  }

}