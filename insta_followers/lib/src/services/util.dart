import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Util {
  String calculateFormat(int num) {
    if (num > 1000000) {
      int tempM = num ~/ 1000000;
      if (num % 1000000 > 100000) {
        int temp = num - (tempM * 1000000);
        int tempK = temp ~/ 100000;
        return '$tempM.$tempK M';
      } else {
        return '$tempM M';
      }
    } else if (num > 10000) {
      int tempK = num ~/ 1000;
      return '$tempK K';
    } else {
      return '$num';
    }
  }

  showSnackBar(BuildContext context, text) {
    final scaffold = ScaffoldMessenger.of(context);
    scaffold.showSnackBar(SnackBar(
      content: Text(text),
      margin: EdgeInsets.only(bottom: 50),
      behavior: SnackBarBehavior.floating,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
      duration: Duration(milliseconds: 2000),
      backgroundColor: Colors.deepOrange,
    ));
  }

  showMyDialog(BuildContext context) {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return CupertinoAlertDialog(
          title: Center(
              child: Text(
            '!Account block warning!',
            style: TextStyle(color: Colors.red),
          )),
          content: SingleChildScrollView(
            padding: EdgeInsets.only(top: 5),
            child: ListBody(
              children: <Widget>[
                Text(
                    "Don't unfollow too much person in a short period of time\n"
                    "to avoid of blocking by Instagram."),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('Understand'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}
