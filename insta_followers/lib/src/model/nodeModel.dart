
class Edges {
  final PageInfo info;
  final List<NodeModel> edges;

  Edges({this.info,this.edges});

  factory Edges.fromJson(Map<String, dynamic> parsedJson) {
    var list = parsedJson['edges'] as List;
    List<NodeModel> nodeList =
        list.map((i) => NodeModel.fromJson(i)).toList();
    return Edges(
      edges: nodeList,
      info: PageInfo.fromJson(parsedJson['page_info'])
    );
  }
}

class NodeModel {
  final String id;
  final String userName;
  final String fullName;
  final String profilePicUrl;

  NodeModel({this.id,this.userName,this.fullName,this.profilePicUrl});

  factory NodeModel.fromJson(Map<String, dynamic> parsedJson) {
    return NodeModel(
        id: parsedJson['node']['id'],
        userName: parsedJson['node']['username'],
        fullName: parsedJson['node']['full_name'],
        profilePicUrl: parsedJson['node']['profile_pic_url']
    );
  }
}

class PageInfo {
  final bool hasNextPage;
  final String endCursor;

  PageInfo({this.hasNextPage,this.endCursor});

  factory PageInfo.fromJson(Map<String, dynamic> parsedJson) {
    return PageInfo(
      hasNextPage: parsedJson['has_next_page'],
      endCursor: parsedJson['end_cursor'],
    );
  }
}