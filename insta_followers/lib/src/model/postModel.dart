
class PostModel {
  final String status;
  final List<Post> data;

  PostModel({this.status, this.data});
  factory PostModel.fromJson(Map<String, dynamic> parsedJson) {
    var list = parsedJson['data'] as List;
    List<Post> postList =
        list.map((i) => Post.fromJson(i)).toList();
    return PostModel(
        status: parsedJson['status'],
        data: postList
    );
  }

}

class Post {
  final String thumbnailUrl;
  final String postId;
  final String authorName;
  final int createdat;

  Post({this.thumbnailUrl, this.postId, this.authorName, this.createdat});
  factory Post.fromJson(Map<String, dynamic> parsedJson) {
    return Post(
        thumbnailUrl: parsedJson['thumbnail_url'],
        postId: parsedJson['id'],
        authorName: parsedJson['username'],
        createdat: parsedJson['created_at']
    );
  }
}