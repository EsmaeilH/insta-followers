
class UserModel {
  final String status;
  final List<User> data;

  UserModel({this.status, this.data});
  factory UserModel.fromJson(Map<String, dynamic> parsedJson) {
    var list = parsedJson['data'] as List;
    List<User> userList =
    list.map((i) => User.fromJson(i)).toList();
    return UserModel(
        status: parsedJson['status'],
        data: userList
    );
  }
}

class User {
  final String userName;
  final String description;
  final int followers;
  final int following;
  final String imgUrl;
  final int createdat;

  User({this.userName, this.description, this.following, this.followers, this.imgUrl, this.createdat});

  factory User.fromJson(Map<String, dynamic> parsedJson) {
    return User(
      userName: parsedJson['username'],
      description: parsedJson['description'],
      followers: parsedJson['followers'],
      following: parsedJson['following'],
      imgUrl: parsedJson['image_url'],
      createdat: parsedJson['created_at']
    );
  }
}