
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:insta_followers/src/services/ad_state.dart';
import 'package:insta_followers/src/services/globalVariable.dart';
import 'package:insta_followers/src/ui/screens/about.dart';
import 'package:insta_followers/src/ui/screens/loginPage.dart';
import 'package:insta_followers/src/services/util.dart';
import 'package:insta_followers/src/services/storageManager.dart';
import 'package:provider/provider.dart';

class MyAppBar extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MyAppBarState();
  }
}

class MyAppBarState extends State<MyAppBar> {

  int _followers = 0, _following = 0;
  String _imgUrl = "";

  final promoteTxtController = TextEditingController();

  InterstitialAd interstitialFollowerList;

  readDataFromPhone() async{
    _followers = (await storageInstance.readDataPref('followers') ?? 0);
    _following = (await storageInstance.readDataPref('following') ?? 0);
    _imgUrl = (await storageInstance.readDataPref('imageUrl'));
  }

  @override
  void initState() {
    super.initState();
    readDataFromPhone();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    final adState = Provider.of<AdState>(context);
    adState.initialization.then((status) {
      interstitialFollowerList = InterstitialAd(
          adUnitId: adState.interstitialFollowerAdUnitId,
          listener: AdListener(
              onAdLoaded: (_) {
                interstitialFollowerList.show();
              },
              onAdFailedToLoad: (_ ,err) {
                interstitialFollowerList?.dispose();
              },
              onAdClosed: (_) {
                interstitialFollowerList?.dispose();
              }
          ),
          request: AdRequest()
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(30),
              bottomRight: Radius.circular(30)),
          color: const Color(0xff151a30)),
      child: Container(
        padding: const EdgeInsets.fromLTRB(15, 20, 15, 0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                GestureDetector(
                  onTap: () {
                    FirebaseAnalytics()
                        .logEvent(name: 'loginPageBtn', parameters: null);
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => LoginPage()));
                  },
                  child: Container(
                    height: 40,
                    width: 40,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                          fit: BoxFit.fill,
                          image: AssetImage('assets/Icon/Icon.png'),
                        )),
                  ),
                ),
                Text(
                  '$instaID',
                  style: TextStyle(color: Colors.white, fontSize: 20),
                ),
                GestureDetector(
                  child: Container(
                    height: 30,
                    width: 30,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            image:
                                AssetImage('assets/Icon/question-mark.png'))),
                  ),
                  onTap: () {
                    FirebaseAnalytics()
                        .logEvent(name: 'questionBtn', parameters: null);
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => About()));
                  },
                )
              ],
            ),
            SizedBox(
              height: 5,
            ),
            Flex(
              direction: Axis.horizontal,
              children: [
                Expanded(
                  child: TextField(
                    controller: promoteTxtController,
                    maxLength: 120,
                    onChanged: (string) {
                      setState(() {
                        promoteTxtController.text;
                      });
                    },
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: Colors.white,
                      hintText: "Tell the others about yourself",
                      counterText: '${promoteTxtController.text.length}/120',
                      counterStyle: TextStyle(
                          color: (promoteTxtController.text.length < 70)
                              ? Colors.white
                              : (promoteTxtController.text.length < 100)
                              ? Colors.deepOrangeAccent
                              : Colors.red[900]),
                    ),
                  ),
                ),
                SizedBox(
                  width: 5,
                ),
                Builder(
                  builder: (context) => GestureDetector(
                    onTap: (){
                      FocusScope.of(context).requestFocus(new FocusNode());
                      if (followCount >= 5) {
                      interstitialFollowerList.load();
                      followCount = 0;
                      isItOkToShow = true;
                      storageInstance.saveUpdateDataDB
                        (instaID, _imgUrl, _followers, _following,
                          promoteTxtController.text == ""
                          ? "Hello, Please follow me!"
                              : promoteTxtController.text,
                          true);
                      promoteTxtController.text = '';
                      final String temp = "You've done it successfully!";
                      Util().showSnackBar(context, temp);
                      setState(() {});
                      } else {
                      final String temp =
                      'You should follow 5 person first! $followCount/5';
                      Util().showSnackBar(context, temp);
                      }
                    },
                    child: Container(
                        height: 47,
                        width: 60,
                        margin: EdgeInsets.only(bottom: 22),
                        decoration: BoxDecoration(
                          // shape: BoxShape.circle,
                          borderRadius: BorderRadius.circular(5),
                          color: Colors.white,
                        ),
                        child: Center(child: Text("Promote", style: TextStyle(color: Colors.deepOrange,)))),
                  ),
                )
              ],
            ),
            SizedBox(
              height: 3,
            ),
          ],
        ),
      ),
    );
  }

  @override
  void dispose(){
    interstitialFollowerList?.dispose();
    promoteTxtController.dispose();
    super.dispose();
  }

}
