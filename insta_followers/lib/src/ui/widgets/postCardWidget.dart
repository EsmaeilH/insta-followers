
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:insta_followers/src/services/globalVariable.dart';
import 'package:insta_followers/src/ui/screens/postList.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:url_launcher/url_launcher.dart';

class PostCardWidget extends StatelessWidget {

  final PostListState _parent;
  final String _thumbnailURL;
  final String _authorName;
  final String _postID;

  PostCardWidget(this._thumbnailURL, this._authorName, this._postID, this._parent);

  final String postUrl = 'https://www.instagram.com/p/';

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        if (!liked.contains(_postID)) {
          likedCount += 1;
          _launchURL(postUrl);
          liked.add(_postID);
          _parent.updateState();
        }else {
          _launchURL(postUrl);
        }
      },
      child: Container(
        height: 230,
        margin: EdgeInsets.fromLTRB(15, 0, 15, 10),
        decoration: BoxDecoration(
          color: Colors.purple,
          borderRadius: BorderRadius.only(topRight: Radius.circular(10), topLeft: Radius.circular(10)),
            border: _parent.winnerPost == _postID
                ?Border.all(color: Colors.yellowAccent, width: 2)
                :Border.all(color: Colors.red, width: 2)
        ),
        child: Column(
          children: [
            Stack(
              alignment: Alignment.center,
              children: [
                Container(
                  height: 200,
                  decoration: BoxDecoration(
                    shape: BoxShape.rectangle,
                      borderRadius: BorderRadius.only(topRight: Radius.circular(10), topLeft: Radius.circular(10)),
                      image: DecorationImage(
                          fit: BoxFit.cover,
                          image: CachedNetworkImageProvider(_thumbnailURL)
                      )
                  ),
                ),
                Positioned(
                  top: 20,
                  left: 20,
                  child: Container(
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(5)),
                      color: Colors.black26,
                    ),
                    child: Text("A post shared by $_authorName", style: TextStyle(color: Colors.white),),
                  ),
                ),
                Positioned(
                  top: 40,
                  child: Opacity(
                    opacity: liked.contains(_postID)? 0.7 : 0.3,
                    child: Icon(Icons.favorite, color: liked.contains(_postID)? Colors.red : Colors.white, size: 170,)
                  )
                ),
              ],
            ),
            SizedBox(height: 5,),
            Text("View this post on Instagram", style: TextStyle(color: Colors.white),)
          ],
        ),
      ),
    );
  }

  Future<void> _launchURL(String url) async {
    url = url + _postID;
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
  
}