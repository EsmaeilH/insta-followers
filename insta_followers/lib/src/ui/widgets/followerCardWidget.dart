import 'dart:core';

import 'package:flutter/material.dart';
import 'package:expandable_text/expandable_text.dart';
import 'package:insta_followers/src/services/globalVariable.dart';
import 'package:insta_followers/src/ui/screens/followerList.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:insta_followers/src/services/util.dart';

class CardItem extends StatelessWidget{

  final FollowerListState parent;

  final String _imgUrl;
  final String _description;
  final String _instaID;
  final int _followers;
  final int _following;

  CardItem(this._imgUrl, this._followers, this._following, this._description, this._instaID, this.parent);

  final TextStyle textColor = TextStyle(color: Colors.white);

  @override
  Widget build(BuildContext context) {
    return Container(
        width: double.infinity,
        margin: EdgeInsets.fromLTRB(15, 0, 15, 10),
        child: Container(
          decoration: BoxDecoration(
              color: Colors.white10,
              borderRadius: BorderRadius.all(Radius.circular(10)),
              border: parent.winnerUser == _instaID
                  ?Border.all(color: Colors.yellowAccent, width: 2)
                  :Border.all(color: Colors.red, width: 2)
          ),
          padding: EdgeInsets.all(10),
          child: Column(
            children: [
              SizedBox(height: 5,),
              Row(
                children: [
                  Expanded(
                    flex: 1,
                    child: GestureDetector(
                      onTap: () {
                        showDialog(
                            context: context,
                          builder: (_) => Container(
                            decoration: new BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                  fit: BoxFit.contain,
                                    image: NetworkImage(_imgUrl)
                                )
                            ),
                          ),
                        );
                      },
                      child: Container(
                        height: 45,
                        width: 45,
                        decoration: new BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                                // fit: BoxFit.fill,
                                image: NetworkImage(_imgUrl)
                            )
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 4,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('$_instaID', style: TextStyle(color: Colors.white, fontSize: 20),),
                        SizedBox(height: 10,),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            Text(Util().calculateFormat(_followers), style: textColor,),
                            SizedBox(width: 15,),
                            Text(Util().calculateFormat(_following), style: textColor,),
                          ],
                        ),
                      ],
                    ),
                  )
                ],
              ),
              Container(
                  width: double.infinity,
                  margin: EdgeInsets.fromLTRB(15, 0, 15, 0),
                  padding: EdgeInsets.fromLTRB(0, 10, 0, 10),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(10))
                  ),
                  child: ExpandableText(
                    _description,
                    expandText: 'show more',
                    collapseText: 'show less',
                    maxLines: 2,
                    linkColor: Colors.blue,
                    style: textColor,
                  )
              ),
              Container(
                height: 30,
                width: double.infinity,
                margin: EdgeInsets.fromLTRB(15, 0, 15, 0),
                child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all<Color>(Colors.white),
                    shape: MaterialStateProperty.all<OutlinedBorder>(
                        RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                            side: BorderSide(color: Colors.red)
                        )
                    ),
                  ),
                  onPressed: () {
                    (!followed.contains(_instaID))? followCount += 1 : followCount = followCount;
                    _launchURL(_instaID);
                    // _isFollowed = true;
                    followed.add(_instaID);
                    parent.updateState();
                  },
                  child: Text(!followed.contains(_instaID)? 'Follow' : 'Followed',
                    style: TextStyle(color: !followed.contains(_instaID)? Colors.black : Colors.deepOrange),),
                ),
              ),
            ],
          ),
        ),
      );
  }
}

Future<void> _launchURL(String _instaID) async {
  String url = 'https://www.instagram.com/_u/$_instaID';
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}
