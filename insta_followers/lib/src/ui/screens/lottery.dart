
import 'dart:async';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:insta_followers/src/services/ad_state.dart';
import 'package:insta_followers/src/services/globalVariable.dart';
import 'package:insta_followers/src/services/storageManager.dart';
import 'package:insta_followers/src/services/util.dart';
import 'package:provider/provider.dart';

import 'myApp.dart';

Color bgColor = Color(0xff222831);
Color darkColor = Color(0xff151a30);

class Lottery extends StatefulWidget {

  final MyAppState _parent;
  Lottery(this._parent);

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return LotteryState();
  }

}

class LotteryState extends State<Lottery> {

  int _numberOfSeenedAd = 0;
  String _time = "0:0:0";
  Timer _timeToNextRound, _videoAdsWaitingTime;

  RewardedAd rewardedLottery;

  @override
  void initState() {
    super.initState();
    checkSeenTime();
    _timeToNextRound = new Timer.periodic(Duration(seconds: 1), (_) => {timeToNextRound()});
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    final adState = Provider.of<AdState>(context);
    adState.initialization.then((status) {
      rewardedLottery = RewardedAd(
          adUnitId: adState.rewardedLotteryAdUnit,
          listener: AdListener(
              onAdLoaded: (_) {
                print("ad loaded");
                Navigator.pop(context);
                _videoAdsWaitingTime?.cancel();
                rewardedLottery.show();
              },
              onAdClosed: (_) {
                rewardedLottery?.dispose();
              },
              onRewardedAdUserEarnedReward: (_ ,RewardItem item) {
                rewardedLottery?.dispose();
                addToLottery();
                setState(() {});
              }
          ),
          request: AdRequest()
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgColor,
      body: SingleChildScrollView(
        padding: EdgeInsets.fromLTRB(20, 50, 20, 50),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            // timer to next round
            Center(
              child: Text("Time left to next round", style: TextStyle(color: Colors.greenAccent, fontSize: 20),),
            ),
            SizedBox(height: 5,),
            Center(
              child: Text(_time, style: TextStyle(color: Colors.greenAccent, fontSize: 20),),
            ),
            // description
            SizedBox(height: 20,),
            Container(
              child: RichText(
                  text: TextSpan(
                    text: '• ',
                    style: TextStyle(color: Colors.deepOrange, fontSize: 17),
                    children: <TextSpan>[
                      TextSpan(
                        text:
                        "In this section you can gain ticket to lottery that can make you stay at the top of ID"
                          " or post list in the next 4 hours.",
                        style: TextStyle(color: Colors.white, fontSize: 17, height: 1.5),
                      ),
                      TextSpan(text: '\n• ',
                          children: <TextSpan>[
                            TextSpan(text:
                         "In order to gain a ticket you should watch a video completely after that you will join the next lottery.",
                              style: TextStyle(color: Colors.white, fontSize: 17, height: 1.5),
                            ),
                          ]
                      ),
                    ],
                  )),
            ),
            SizedBox(height: 50,),
            // video ads button
            Center(
              child: ElevatedButton(
                  onPressed: () {
                    if(_numberOfSeenedAd < 2 && postIDG != null || _numberOfSeenedAd < 1) {
                      rewardedLottery.load();
                      // Ads fail waiting time
                      _videoAdsWaitingTime = Timer(
                          Duration(seconds: 10),
                          () {
                            Navigator.pop(context);
                            Util().showSnackBar(context, "Failed to load video, Try again later.");
                            rewardedLottery?.dispose();
                          }
                      );
                      // Ads waiting dialog
                      showDialog(
                          context: context,
                          barrierDismissible: false,
                          builder: (_) => Dialog(
                            child: Container(
                              color: Colors.pink,
                              padding: EdgeInsets.fromLTRB(20, 7, 7, 7),
                              height: 50,
                              child: Center(
                                child: Row(
                                  children: [
                                    CircularProgressIndicator(),
                                    SizedBox(width: 30,),
                                    Text("Waiting to load video...", style: TextStyle(color: Colors.white),)
                                  ],
                                ),
                              ),
                            ),
                          )
                      );
                    } else if(postIDG == null) {
                      showDialog(
                          context: context,
                          builder: (_) => Dialog(
                            child: Container(
                              color: Colors.pink,
                              padding: EdgeInsets.fromLTRB(20, 7, 7, 7),
                              height: 120,
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  RichText(
                                      text: TextSpan(
                                        text: "• You don't have any shared post yet, you should first share a post.",
                                        style: TextStyle(color: Colors.white, fontSize: 15),
                                      )
                                  ),
                                  SizedBox(height: 20,),
                                  ElevatedButton(
                                      onPressed: () {
                                        widget._parent.setState(() {
                                          Navigator.pop(context);
                                          widget._parent.selectedIndex = 1;
                                        });
                                      },
                                      child: Text("Share a post")
                                  )
                                ],
                              ),
                            ),
                          )
                      );
                    } else {
                      Util().showSnackBar(context, "You have earned all the ticket, wait for the next round.");
                    }
                  },
                  child: Icon(Icons.play_circle_outline_sharp, size: 50, color: Colors.pink,)
              ),
            ),
            SizedBox(height: 50,),
            // video seen status
            Container(
              child: RichText(
                  text: TextSpan(
                    text: '• ',
                    style: TextStyle(color: Colors.deepOrange, fontSize: 17),
                    children: <TextSpan>[
                      _numberOfSeenedAd == 0?
                      TextSpan(
                        text:
                        "You have no ticket yet, try to gain at least one.",
                        style: TextStyle(color: Colors.white, fontSize: 17, height: 1.5),
                      )
                          :
                      _numberOfSeenedAd == 1?
                      TextSpan(
                        text:
                        "You have earned your profile list ticket, you can watch one more to get post list ticket either.",
                        style: TextStyle(color: Colors.white, fontSize: 17, height: 1.5),
                      )
                          :
                      TextSpan(
                        text:
                        "You have earned your tickets, now wait for the result.",
                        style: TextStyle(color: Colors.deepOrange, fontSize: 17, height: 1.5),
                      ),
                    ],
                  )),
            ),
          ],
        ),
      )
    );
  }

  @override
  void dispose() {
    super.dispose();
    rewardedLottery?.dispose();
    _timeToNextRound?.cancel();
    _videoAdsWaitingTime?.cancel();
  }

  void checkSeenTime() async{
    _numberOfSeenedAd = await storageInstance.readDataPref("numberofseenedad") ?? 0;
    String _timeOfSeen = await storageInstance.readDataPref("timeofseen") ?? "2000-03-13 18:00:00.000Z";
    if(DateTime.parse(_timeOfSeen).toUtc().day == DateTime.now().toUtc().day) {
      int endIntervalTime = ((DateTime.parse(_timeOfSeen).toUtc().hour ~/ 6) * 6) + 6;
      if(DateTime.now().toUtc().hour >= endIntervalTime) {
        _numberOfSeenedAd = 0;
        storageInstance.saveDataPref("numberofseenedad", _numberOfSeenedAd);
      }else{
        setState(() {

        });
      }
    }else {
      _numberOfSeenedAd = 0;
      storageInstance.saveDataPref("numberofseenedad", _numberOfSeenedAd);
    }
  }

  void timeToNextRound() {
    int endIntervalTime = ((DateTime.now().toUtc().hour ~/ 6) * 6) + 6;
    // Duration _temp;
    Duration _temp = DateTime.utc(DateTime.now().toUtc().year, DateTime.now().toUtc().month, DateTime.now().toUtc().day, endIntervalTime, 0, 0).difference(DateTime.now().toUtc());
    setState(() {
      if(_temp.inSeconds == 0) {
        _numberOfSeenedAd = 0;
      }
      _time = _temp.toString().split(".")[0];
    });
  }

  void addToLottery() async{
    if(_numberOfSeenedAd == 0) {
      try{
        Response response = await storageInstance.addUserToLotteryList(instaID);
        if(response.statusCode == 200){
          _numberOfSeenedAd++;
          storageInstance.saveDataPref("numberofseenedad", _numberOfSeenedAd);
          storageInstance.saveDataPref("timeofseen", DateTime.now().toUtc().toString());
        }
      }on DioError catch(err) {
        print("this is err ${err.response.statusCode}");
        print("this is err ${err.response.statusMessage}");
        Util().showSnackBar(context, "Sorry error happened. try again later!");
      }
    } else {
      try{
        Response response = await storageInstance.addPostToLotteryList(postIDG);
        if(response.statusCode == 200){
          _numberOfSeenedAd++;
          storageInstance.saveDataPref("numberofseenedad", _numberOfSeenedAd);
          storageInstance.saveDataPref("timeofseen", DateTime.now().toUtc().toString());
        }
      }on DioError catch(err) {
        print("this is err ${err.response.statusCode}");
        print("this is err ${err.response.statusMessage}");
        Util().showSnackBar(context, "Sorry error happened. try again later!");
      }
    }
  }

}