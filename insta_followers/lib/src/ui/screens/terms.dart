
import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:insta_followers/src/ui/screens/loginWebView.dart';
import 'package:url_launcher/url_launcher.dart';

class Terms extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return TermsState();
  }

}

class TermsState extends State<Terms>{

  bool checkBoxValue = false;

  Future<void> _launchURL() async {
    String url = 'https://insta-followers-e1a03.web.app/terms.html';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff222831),
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(150),
          child: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(30),
                    bottomRight: Radius.circular(30)),
                color: const Color(0xff151a30)),
            child: Center(child: Text('Privacy Policy', style: TextStyle(color: Colors.white, fontSize: 25, height: 1.5),)),
          )),
      body: Flex(
        direction: Axis.vertical,
        children: [
          Expanded(
            child: SingleChildScrollView(
              child: Container(
                  padding: EdgeInsets.only(left: 20, right: 20, top: 50, bottom: 50),
                  child: RichText(
                      text: TextSpan(
                          text: '• ',
                          style: TextStyle(color: Colors.deepOrange, fontSize: 17, height: 1.5),
                          children: <TextSpan>[
                            TextSpan(text:
                            'By accessing and/or using Darchini Studio products or services, '
                                'you are accepting the practices described in this Privacy Policy. '
                                'Darchini Studio strongly believes in keeping your personal information confidential.',
                              style: TextStyle(color: Colors.white, fontSize: 17, height: 1.5),
                            ),
                            TextSpan(text: '\n• ',
                                children: <TextSpan>[
                                  TextSpan(text:
                                  'Darchini Studio do not collect any personal information from you when you download our android/ios applications. '
                                      'We do not require the users to get registered before downloading the application '
                                      'and we do keep track of the users visits of our applications.',
                                    style: TextStyle(color: Colors.white, fontSize: 17, height: 1.5),
                                  ),
                                ]
                            ),
                            TextSpan(text: '\n• Use Of Collected Information',
                                children: <TextSpan>[
                                  TextSpan(text:
                                  '\nWe do not collect any personal or sensitive information from our users, the data that we collect from you is '
                                      'limited to your Instagram username, count of follower & following and profile image.'
                                      'The only situation we may get access to your personal data is when you decide to email us '
                                      'your feedback.'
                                      'The data we may get from you in that situation are limited to your name, email address and your feedback.'
                                      ' Darchini Studio guarantee that your data will only be used for contacting you and improving our services.'
                                      ' We will never use such information for any other purpose, such as to further market our products, '
                                      'or to disclose your personal information to a third party for commercial gains.',
                                    style: TextStyle(color: Colors.white, fontSize: 17, height: 1.5),
                                  ),
                                ]
                            ),
                            TextSpan(text: '\n• Age Limits',
                                children: <TextSpan>[
                                  TextSpan(text:
                                  '\nWe do not knowingly collect or solicit personal data about or direct or target interest based '
                                      'advertising to anyone under the age of 13 or knowingly allow such persons to use our Services. '
                                      'If you are under 13, please do not send any data about yourself to us, including your name, '
                                      'address, telephone number, or email address. No one under the age of 13 may provide any personal data.'
                                      ' If we learn that we have collected personal data about a child under age 13, we will delete that data.',
                                    style: TextStyle(color: Colors.white, fontSize: 17, height: 1.5),
                                  ),
                                ]
                            ),
                            TextSpan(text: '\n• Third Party Advertising',
                                children: <TextSpan>[
                                  TextSpan(text:
                                  '\nOur free applications use advertising currently provided by Admob and anonymous statistical information'
                                      ' is reported to them so they can see whether their adverts are effective. '
                                      'for more information visit admob website.',
                                    style: TextStyle(color: Colors.white, fontSize: 17, height: 1.5),
                                  ),
                                ]
                            ),
                            TextSpan(text: '\n• Changes In Our Privacy Policy',
                                children: <TextSpan>[
                                  TextSpan(text:
                                  '\nDarchini Studio reserves the right to update and revise this privacy policy from time to time '
                                      'to reflect changes in legislation or any other developments.'
                                      'check this link to get latest ',
                                    style: TextStyle(color: Colors.white, fontSize: 17, height: 1.5),
                                    children: <TextSpan>[
                                      TextSpan(
                                        text: 'Privacy Policy',
                                        style: TextStyle(color: Colors.lightBlue),
                                        recognizer: TapGestureRecognizer()..onTap = () {
                                          _launchURL();
                                        }
                                      )
                                    ]
                                  ),
                                ]
                            ),
                            TextSpan(text: '\n• Contact Us',
                                children: <TextSpan>[
                                  TextSpan(text:
                                  '\nIf you have any questions, complaints, or comments regarding this Privacy Policy or our information '
                                      'collection practices, please contact us.',
                                    style: TextStyle(color: Colors.white, fontSize: 17, height: 1.5),
                                  ),
                                ]
                            )
                          ]
                      )
                  )
              ),
            ),
          ),
          Container(
            height: 100,
            width: double.infinity,
            color: const Color(0xff151a30),
            child: Flex(
              direction: Axis.vertical,
              children: [
                Row(
                  children: [
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Theme(
                        child: Checkbox(
                          value: checkBoxValue,
                          onChanged: (value) {
                            setState(() {
                              checkBoxValue = value;
                            });
                          },
                        ),
                        data: ThemeData(
                          primarySwatch: Colors.blue,
                          unselectedWidgetColor: Colors.deepOrangeAccent, // Your color
                        ),
                      ),
                    ),
                    Text('I agree with this terms', style: TextStyle(color: Colors.white),)
                  ],
                ),
                Expanded(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Expanded(
                        child: ElevatedButton(
                          style: ButtonStyle(
                            backgroundColor: MaterialStateProperty.all<Color>(Colors.white)
                          ),
                          onPressed: () {
                            // _logOutProcess();
                            Navigator.pop(context);
                          },
                          child: Text('CANCEL',
                            style: TextStyle(color: Colors.deepOrange),),
                        ),
                      ),
                      Expanded(
                        child: ElevatedButton(
                          style: ButtonStyle(
                              backgroundColor: checkBoxValue? MaterialStateProperty.all<Color>(Colors.white) : null
                          ),
                          onPressed: checkBoxValue? (){
                            Navigator.pushReplacement(
                                context,
                                MaterialPageRoute(builder: (context) => LoginWebView())
                            );
                          }
                              :
                          null,
                          child: Text('AGREE',
                            style: TextStyle(color: Colors.deepOrange),),
                        ),
                      ),
                    ],
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

}