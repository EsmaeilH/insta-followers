import 'dart:async';

import 'package:dio/dio.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:insta_followers/src/services/ad_state.dart';
import 'package:insta_followers/src/services/globalVariable.dart';
import 'package:insta_followers/src/services/instagramAPI.dart';
import 'package:provider/provider.dart';
import 'myApp.dart';
import 'package:insta_followers/src/services/storageManager.dart';
import 'package:insta_followers/src/services/util.dart';

class LoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LoginPageState();
  }
}

class LoginPageState extends State<LoginPage> {
  final myController = TextEditingController();
  bool isTapped = false, isErrVisible = false;
  bool _isInterstitialAdReady = false;
  String errMessage;

  InterstitialAd interstitialLogin;

  _logOutProcess() async{
    storageInstance.saveDataPref('isLoggedIn', false);
    await PrepareJson().logOutRequest();
    differencesMap.clear();
    isLogin = false;
    cookie = '';
    csrftoken = '';
    // Setting obj = await storageInstance.getSettingFromServer();
    cookie = storageInstance.instaCookie;
    setState(() {

    });
  }

  void _isExistingUser(String id) {
    if(id != 'undefined') {
      difference = 0;
      isItOkToShow = false;
    }
  }

  Future firstConfig() async {
    try {
      if (cookie == "") {
        isErrVisible = true;
        throw Exception(
            'Connectivity issue, check if you are connected then try again!');
      }
      Response response =
          await PrepareJson().getUserProfileJson(myController.text.trim().toLowerCase(), cookie).timeout(Duration(seconds: 6));
      String _description = "Hello, Please follow me!";
      String _imgUrl = response.data['graphql']['user']['profile_pic_url_hd'];
      String _userId = response.data['graphql']['user']['id'];
      int _followers =
          response.data['graphql']['user']['edge_followed_by']['count'];
      int _following = response.data['graphql']['user']['edge_follow']['count'];
      _isExistingUser(instaID);
      storageInstance.saveDataPref('seen', true);
      storageInstance.saveDataPref('instaID', myController.text.trim().toLowerCase());
      storageInstance.saveDataPref('id', _userId);
      storageInstance.saveDataPref('followers', _followers);
      storageInstance.saveDataPref('following', _following);
      storageInstance.saveDataPref('imageUrl', _imgUrl);
      if (instaID != myController.text.trim().toLowerCase()){
        instaID = myController.text.trim().toLowerCase();
        // storageInstance.saveUpdateDataDB
        //   (instaID, _imgUrl, _followers, _following, _description, true);
      }
    } catch (err) {
      if (err.toString().split(': ').last == 'statusCode') {
        errMessage =
            'Connectivity issue, check if you are connected then try again!';
      } else {
        errMessage = 'Instagram not respond, try again';
      }
      setState(() {
        isTapped = false;
      });
    }
  }

  void goToMainPage() {
    Navigator.pushReplacement(
        context,
        MaterialPageRoute(
            builder: (context) => MyApp()));
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    final adState = Provider.of<AdState>(context);
    adState.initialization.then((status) {
      interstitialLogin = InterstitialAd(
          adUnitId: adState.interstitialLoginAdUnitId,
          listener: AdListener(
            onAdLoaded: (_) {
              setState(() {
                _isInterstitialAdReady = true;
              });
            },
            onAdClosed: (_) {
              Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                      builder: (context) => MyApp()));
              _isInterstitialAdReady = false;
            }
          ),
          request: AdRequest()
      )..load();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: const Color(0xff151a30),
        body: isLogin?
            //login state
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Center(
              child: Text(
                'IG Followers',
                style: TextStyle(color: Colors.white, fontSize: 25),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Center(
              child: Container(
                height: 100,
                width: 100,
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                        fit: BoxFit.fill,
                        image: AssetImage('assets/Icon/insta1920.png'))),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Center(
              child: Text('$instaID',style: TextStyle(color: Colors.white, fontSize: 20),),
            ),
            SizedBox(
              height: 30,
            ),
            Container(
              height: 50,
              width: double.infinity,
              margin: EdgeInsets.fromLTRB(15, 0, 15, 0),
              child: ElevatedButton(
                style: ButtonStyle(
                  backgroundColor: MaterialStateProperty.all<Color>(Colors.white),
                  shape: MaterialStateProperty.all<OutlinedBorder>(
                      RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          side: BorderSide(color: Colors.red)
                      )
                  ),
                ),
                onPressed: () {
                  _logOutProcess();
                  Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(builder: (context) => MyApp())
                  );
                },
                child: Text('Logout',
                  style: TextStyle(color: Colors.deepOrange),),
              ),
            ),
          ],
        )
            :
            //logout state
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Center(
              child: Text(
                'IG Followers',
                style: TextStyle(color: Colors.white, fontSize: 25),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Center(
              child: Container(
                height: 100,
                width: 100,
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                        fit: BoxFit.fill,
                        image: AssetImage('assets/Icon/insta1920.png'))),
              ),
            ),
            SizedBox(
              height: 50,
            ),
            Flex(
              direction: Axis.horizontal,
              children: [
                SizedBox(
                  width: 20,
                ),
                Expanded(
                  flex: 4,
                  child: TextField(
                    maxLength: 30,
                    controller: myController,
                    onChanged: (string) {
                      setState(() {});
                    },
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: Colors.white,
                      hintText: "Enter your Instagram username",
                      counterText: '${myController.text.length}/30',
                      counterStyle: TextStyle(
                          color: (myController.text.length < 15)
                              ? Colors.white
                              : (myController.text.length < 25)
                                  ? Colors.deepOrangeAccent
                                  : Colors.red[900]),
                    ),
                  ),
                ),
                SizedBox(width: 20,),
                Builder(
                  builder: (context) => GestureDetector(
                    onTap: () async {
                      FocusScope.of(context).requestFocus(new FocusNode());
                      FirebaseAnalytics()
                          .logEvent(name: 'loginBtn', parameters: null);
                      if (myController.text.trim() != "") {
                        setState(() {
                          isTapped = true;
                        });
                        await firstConfig();
                        if (isTapped) {
                          (!_isInterstitialAdReady)?
                          Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => MyApp()))
                              :
                          interstitialLogin.show();
                        } else {
                          Util().showSnackBar(context, errMessage);
                        }
                      } else {}
                    },
                    child: Container(
                        height: 45,
                        width: 45,
                        margin: EdgeInsets.only(bottom: 22),
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: Colors.white,
                        ),
                        child: Icon(Icons.navigate_next)),
                  ),
                ),
                SizedBox(width: 20,),
              ],
            ),
            (isTapped)
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : Center(),
            Container(
              padding: EdgeInsets.fromLTRB(20, 50, 20, 20),
              child: RichText(
                  text: TextSpan(
                text: '• ',
                style: TextStyle(color: Colors.deepOrange, fontSize: 15),
                children: <TextSpan>[
                  TextSpan(
                    text:
                        'Be careful to enter your Instagram username correctly!',
                    style: TextStyle(color: Colors.white, fontSize: 15),
                  ),
                ],
              )),
            ),
            Opacity(
              opacity: (!isErrVisible) ? 0.0 : 1.0,
              child: Container(
                padding: EdgeInsets.fromLTRB(20, 0, 20, 20),
                child: RichText(
                    text: TextSpan(
                      text: '• ',
                      style: TextStyle(color: Colors.deepOrange, fontSize: 15),
                      children: <TextSpan>[
                        TextSpan(
                          text:
                          'If you are seeing this message then you should probably close the app and try again.',
                          style: TextStyle(color: Colors.deepOrange, fontSize: 15),
                        ),
                      ],
                    )),
              ),
            )
          ],
        ));
  }

  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.
    myController.dispose();
    interstitialLogin?.dispose();
    super.dispose();
  }
}
