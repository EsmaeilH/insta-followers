
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:insta_followers/src/services/globalVariable.dart';
import 'package:insta_followers/src/services/storageManager.dart';
import 'package:insta_followers/src/ui/screens/myApp.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';

class LoginWebView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LoginWebViewState();
  }

}

class LoginWebViewState extends State<LoginWebView> {
  CookieManager cookieManager = CookieManager();
  String url = "https://instagram.com/accounts/login";

  void initState() {
    super.initState();
    themeChangeHandler();
  }

  void themeChangeHandler () {
    setState(() {
      SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
          statusBarBrightness: Brightness.dark));
    });
  }

  getCookie() async{
    var cookies = await cookieManager.getCookies(url: url);
    cookie = "";
    for(var item in cookies){
      if(item.name == 'csrftoken'){
        storageInstance.saveDataPref('csrftoken', item.value);
        csrftoken = item.value;
      }
      cookie += item.name + '=' + item.value + '; ';
    }
    storageInstance.saveDataPref('cookie', cookie);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(150),
        child: Container(
          height: 30,
          color: const Color(0xff151a30),
        ),
      ),
      body: InAppWebView(
        initialUrl: url,
        initialOptions: InAppWebViewGroupOptions(
            crossPlatform: InAppWebViewOptions(
              cacheEnabled: true,
              javaScriptEnabled: true,
            )
        ),
        onLoadStart: (InAppWebViewController controller, url) async{
          print(url);
          if(url.toString().startsWith('https://www.instagram.com/accounts/onetap/') ||
          url.toString() == 'https://www.instagram.com/') {
            storageInstance.saveDataPref('isLoggedIn', true);
            isLogin = true;
            await getCookie();
            Navigator.pushReplacement(
              context,
              MaterialPageRoute(builder: (context) => MyApp())
            );
          }
        },
      ),
    );
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    cookieManager.deleteAllCookies();
  }

}