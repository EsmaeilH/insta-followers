import 'dart:async';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:insta_followers/src/ui/screens/forceUpdate.dart';
import 'package:package_info/package_info.dart';
import 'package:in_app_update/in_app_update.dart';
import 'package:insta_followers/src/services/globalVariable.dart';
import 'package:insta_followers/src/services/instagramAPI.dart';
import 'package:insta_followers/src/ui/screens/loginPage.dart';

import '../../services/storageManager.dart';
import 'myApp.dart';

class SplashScreen extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return SplashScreenState();
  }
}

class SplashScreenState extends State<SplashScreen> {
  String _latestVersion, _version;
  String _forceUpdate;

  AppUpdateInfo _updateInfo;

  void initState() {
    super.initState();
    themeChangeHandler();
    checkForUpdate();
    checkFirstSeen();
    PackageInfo.fromPlatform().then((PackageInfo packageInfo) {
      _version = packageInfo.version;
    });
  }

  Future<void> checkForUpdate() async {
    InAppUpdate.checkForUpdate().then((info) {
      setState(() {
        _updateInfo = info;
      });
    }).catchError((e) {
      print("in app update failed");
    });
  }

  getSetting() async{
    isLogin = await storageInstance.readDataPref('isLoggedIn') ?? false;
    try {
      await storageInstance.getSettingFromServer();
      if(!isLogin){
        cookie = storageInstance.instaCookie;
        storageInstance.saveDataPref('cookie', cookie);
      }else{
        cookie = await storageInstance.readDataPref('cookie');
      }
      storageInstance.saveDataPref('followerQueryHash', storageInstance.followerQueryHash);
      storageInstance.saveDataPref('followingQueryHash', storageInstance.followingQueryHash);
      _forceUpdate = storageInstance.forceUpdate;
      _latestVersion = storageInstance.latestVersion;
    } catch(err) {
      print("fetch setting err $err");
    }
  }

  void themeChangeHandler () async{
    setState(() {
      SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
          statusBarBrightness: Brightness.dark));
    });
  }

  Future checkFirstSeen() async {
    bool _seen = (await storageInstance.readDataPref('seen') ?? false);
    await getSetting();
    if (_seen) {
      await startUpConfig();
      (_forceUpdate == 'true' && _version != _latestVersion && _updateInfo?.updateAvailability == UpdateAvailability.updateAvailable)?
        InAppUpdate.performImmediateUpdate()
          .catchError((e) => Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => new ForceUpdate())))
          :
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => new MyApp()));
    } else {
      //waiting to complete reading cookie from server
      Timer(new Duration(seconds: 2), () {
        Navigator.pushReplacement(
            context, MaterialPageRoute(builder: (context) => new LoginPage()));
      });
    }
  }

  startUpConfig() async {
    try{
      instaID = (await storageInstance.readDataPref('instaID'));
      String _cookie = (await storageInstance.readDataPref('cookie'));
      int _lastFollowers = (await storageInstance.readDataPref('followers'));
      Response response = await PrepareJson().getUserProfileJson(instaID, _cookie);
      String _imgUrl = response.data['graphql']['user']['profile_pic_url_hd'];
      int _followers =
      response.data['graphql']['user']['edge_followed_by']['count'];
      int _following = response.data['graphql']['user']['edge_follow']['count'];
      difference = _followers - _lastFollowers;
      storageInstance.saveDataPref('followers', _followers);
      storageInstance.saveDataPref('following', _following);
      storageInstance.saveDataPref('imageUrl', _imgUrl);
      storageInstance.saveUpdateDataDB(instaID, _imgUrl, _followers, _following, "", false);
    }catch(err) {
      print('splash screen error is: $err');
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        backgroundColor: const Color(0xff151a30),
        body: Center(
          child: ClipOval(
            child: Container(
              height: 300,
              width: 300,
              child: Image.asset('assets/Icon/Icon.png', fit: BoxFit.fill),
            ),
          ),
        ));
  }
}
