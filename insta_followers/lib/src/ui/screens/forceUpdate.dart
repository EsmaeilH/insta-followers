
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class ForceUpdate extends StatelessWidget {

  Future<void> _launchURL() async {
    String url = 'https://play.google.com/store/apps/details?id=com.myproject.followersforinstagram';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xff151a30),
      body: Container(

        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            GestureDetector(
              onTap: () async {
                _launchURL();
              },
              child: Center(
                child: Container(
                    height: 70,
                    width: 70,
                    margin: EdgeInsets.only(bottom: 22),
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: Colors.white,
                    ),
                    child: Icon(Icons.system_update_tv)),
              )
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(20, 0, 20, 0),
              child: RichText(
                  text: TextSpan(
                    text: '• ',
                    style: TextStyle(color: Colors.white, fontSize: 20),
                    children: <TextSpan>[
                      TextSpan(
                        text:
                        'You need to update before continue!',
                        style: TextStyle(color: Colors.deepOrange, fontSize: 20),
                      )
                    ],
                  )),
            ),
          ],
        ),
      ),
    );
  }

}