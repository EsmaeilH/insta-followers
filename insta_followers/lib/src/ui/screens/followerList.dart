import 'dart:async';

import 'package:flutter/material.dart';
import 'package:in_app_review/in_app_review.dart';
import 'package:insta_followers/src/model/userModel.dart';
import 'package:insta_followers/src/services/ad_state.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:insta_followers/src/ui/widgets/MyAppBar.dart';
import 'package:insta_followers/src/ui/widgets/followerCardWidget.dart';
import 'package:insta_followers/src/services/storageManager.dart';
import 'package:insta_followers/src/services/globalVariable.dart';
import 'package:insta_followers/src/services/util.dart';
import 'package:in_app_update/in_app_update.dart';
import 'package:package_info/package_info.dart';
import 'package:provider/provider.dart';

class FollowerList extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return FollowerListState();
  }
}

class FollowerListState extends State<FollowerList> {
  int _followers = 0, _following = 0;
  int selectedIndex = 0;
  String _imgUrl = "";
  String winnerUser;
  Timer timer;
  bool _isBannerAdLoaded = false;
  bool _isUpdateDone = false;
  var _version;
  static const Color darkColor = const Color(0xff151a30);
  Color bgColor = Color(0xff222831);
  final TextStyle textStyle = TextStyle(color: Colors.white, fontSize: 18);
  final InAppReview inAppReview = InAppReview.instance;
  AppUpdateInfo _updateInfo;

  StreamController<List<User>> followerListStreamController = StreamController();

  BannerAd banner;

  @override
  void initState() {
    super.initState();
    readPreferences();
    inAppReviewRequest();
    checkForUpdate();
    prepareList();
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    final adState = Provider.of<AdState>(context);
    adState.initialization.then((statue) {
      setState(() {
        banner = BannerAd(
            size: AdSize.banner,
            adUnitId: adState.bannerAdUnitId,
            listener: AdListener(
              onAdLoaded: (_) {
                print("Ad loaded");
                _isBannerAdLoaded = true;
              },
              onAdFailedToLoad: (ad, error) {
                _isBannerAdLoaded = false;
              },
            ),
            request: AdRequest()
        )..load();
      });
    });
  }

  Future<void> checkForUpdate() async {
    InAppUpdate.checkForUpdate().then((info) {
      setState(() {
        _updateInfo = info;
      });
    }).catchError((e) {
      print("in app update failed");
    });
  }

  Future<void> readPreferences() async{
    _followers = await storageInstance.readDataPref('followers') ?? 0;
    _following = await storageInstance.readDataPref('following') ?? 0;
    _imgUrl = await storageInstance.readDataPref('imageUrl');
    csrftoken = await storageInstance.readDataPref('csrftoken');
    setState(() {});
  }

  inAppReviewRequest() async {
    if (await inAppReview.isAvailable()) {
      inAppReview.requestReview();
    }
    PackageInfo.fromPlatform().then((PackageInfo packageInfo) {
      _version = packageInfo.version;
    });
  }

  prepareList() async {
    _refreshList();
    timer = Timer.periodic(Duration(seconds: 15), (Timer t) => _refreshList());
  }

  Future<Null> _refreshList() async{
    storageInstance.getDataDB().then((value) {
      if(DateTime.fromMillisecondsSinceEpoch(value.data.first.createdat * 1000, isUtc: true).isAfter(DateTime.now().toUtc())) {
        winnerUser = value.data.first.userName;
      }else {
        winnerUser = "";
      }
      followerListStreamController.sink.add(value.data);
    });
  }

  void updateState() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: bgColor,
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(120),
          child: Builder(builder: (context) => MyAppBar())
      ),
      body: Flex(
        direction: Axis.vertical,
        children: [
          SizedBox(
            height: 5,
          ),
          // user info widget
          Container(
            decoration: BoxDecoration(
                color: Colors.white38,
                borderRadius: BorderRadius.all(Radius.circular(10))),
            height: 55,
            margin: EdgeInsets.fromLTRB(15, 0, 15, 0),
            padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  height: 45,
                  width: 45,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                          fit: BoxFit.fill,
                          image: NetworkImage(_imgUrl))),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      Util().calculateFormat(_followers),
                      style: textStyle,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      'Followers',
                      style: textStyle,
                    )
                  ],
                ),
                (difference > 0)
                    ? Row(
                  children: [
                    Text('$difference',
                        style: TextStyle(
                            color: Colors.white, fontSize: 15)),
                    Image.asset(
                      'assets/Icon/arrow-up.png',
                      width: 30,
                    )
                  ],
                )
                    : (difference < 0)
                    ? Row(
                  children: [
                    Image.asset(
                      'assets/Icon/arrow-down.png',
                      width: 30,
                    ),
                    Text('$difference',
                        style: TextStyle(
                            color: Colors.white, fontSize: 15))
                  ],
                )
                    : Center(),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      Util().calculateFormat(_following),
                      style: textStyle,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      'Following',
                      style: textStyle,
                    )
                  ],
                ),
              ],
            ),
          ),
          SizedBox(
            height: 3,
          ),
          // update available widget

          if (_version != storageInstance.latestVersion && _updateInfo?.updateAvailability ==
              UpdateAvailability.updateAvailable)
            Container(
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.all(Radius.circular(10))),
              height: 30,
              margin: EdgeInsets.fromLTRB(15, 0, 15, 0),
              // padding: EdgeInsets.fromLTRB(15, 0, 15, 0),
              child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all<Color>(Colors.white),
                  ),
                  onPressed:
                  // if it is not updated statement
                  !_isUpdateDone? () {
                    InAppUpdate.startFlexibleUpdate().then((_) {
                      setState(() {
                        _isUpdateDone = true;
                        Util().showSnackBar(context, "Reopen the app to apply changes.");
                      });
                    }).catchError((e) {
                      Util().showSnackBar(context, "Failed to get the update!");
                    });
                  }
                      :
                  // else statement
                  Center(),
                  child: Center(
                      child: !_isUpdateDone ? Text(
                        'New update available, click to start update',
                        style: TextStyle(color: Colors.red, fontSize: 15),
                        maxLines: 1,
                      )
                          :
                      Text(
                        'Reopen the app to apply changes',
                        style: TextStyle(color: Colors.red, fontSize: 15),
                        maxLines: 1,
                      )
                  )),
            ),
          SizedBox(
            height: 3,
          ),

          // banner ad place
          _isBannerAdLoaded == false
              ? Center()
              : Container(
            height: 50,
            child: AdWidget(ad: banner,),
          ),
          SizedBox(
            height: 3,
          ),
          // main list section
          Expanded(
            flex: 10,
            child: RefreshIndicator(
              child: StreamBuilder(
                stream: followerListStreamController.stream,
                initialData: List<User>.empty(),
                builder: (context, AsyncSnapshot<List<User>> snapshot) {
                  if(snapshot.connectionState == ConnectionState.waiting){
                    return CircularProgressIndicator();
                  }
                  else if(snapshot.hasData){
                    // userList = snapshot.data;
                    return ListView.builder(
                        itemCount: snapshot.data.length,
                        itemBuilder: (BuildContext context, int index) {
                          List<User> _user = snapshot.data;
                          return CardItem(
                              _user[index].imgUrl,
                              _user[index].followers,
                              _user[index].following,
                              _user[index].description == ''
                                  ? "Hello, Please follow me!"
                                  : _user[index].description,
                              _user[index].userName,
                              this);
                        });
                  }
                  return CircularProgressIndicator();
                },
              ),
              onRefresh: _refreshList,
            ),
          ),
        ],
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    followerListStreamController.close();
    timer?.cancel();
    banner?.dispose();
  }
}
