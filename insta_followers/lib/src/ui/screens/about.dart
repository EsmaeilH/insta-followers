import 'package:flutter/material.dart';

class About extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return AboutState();
  }
}

class AboutState extends State<About> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff222831),
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(150),
          child: Container(
            decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(30),
                    bottomRight: Radius.circular(30)),
                color: const Color(0xff151a30)),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    IconButton(
                        icon: Icon(Icons.arrow_back, color: Colors.white),
                        padding: EdgeInsets.only(left: 20),
                        iconSize: 30,
                        onPressed: () {
                          Navigator.pop(context);
                        }
                    ),
                    Text('IG Followers', style: TextStyle(color: Colors.white, fontSize: 25),),
                    SizedBox(width: 30,)
                  ],
                ),
                SizedBox(height: 20,),
                Container(
                  height: 70,
                  width: 70,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                      image: AssetImage('assets/Icon/Icon.png')
                    )
                  ),
                )
              ],
            ),
          )),
      body: SingleChildScrollView(
        child: Container(
          // alignment: Alignment.centerLeft,
        padding: EdgeInsets.only(left: 20, right: 20, top: 70, bottom: 50),
        child: RichText(
            text: TextSpan(
              text: '• ',
              style: TextStyle(color: Colors.deepOrange, fontSize: 17, height: 1.5),
              children: <TextSpan>[
                TextSpan(text:
                    'This is an app to help people raise their followers on Instagram. '
                        'To reach this purpose you should first follow 5 person then you can promote yourself and come up in the list. '
                        'try to explain what is your account about and encourage others to follow you. ',
                  style: TextStyle(color: Colors.white, fontSize: 17, height: 1.5),
                ),
                TextSpan(text: '\n• ',
                  children: <TextSpan>[
                    TextSpan(text:
                            'If you want to raise your followers, follow as many as you want but keep it in mind that do not follow many account in '
                            'a short period of time or unfollow the accounts that you follow.',
                      style: TextStyle(color: Colors.white, fontSize: 17, height: 1.5),
                    ),
                  ]
                ),
                TextSpan(text: '\n• ',
                    children: <TextSpan>[
                      TextSpan(text:
                      'If you want to get more followers every day do not forget to suggest it to your friends.',
                        style: TextStyle(color: Colors.white, fontSize: 17, height: 1.5),
                      ),
                    ]
                )
              ]
            )
        )
    ),
      ),
    );
  }
}
