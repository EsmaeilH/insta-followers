import 'dart:async';
import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:insta_followers/src/model/postModel.dart';
import 'package:insta_followers/src/services/ad_state.dart';
import 'package:insta_followers/src/services/globalVariable.dart';
import 'package:insta_followers/src/services/instagramAPI.dart';
import 'package:insta_followers/src/services/util.dart';
import 'package:insta_followers/src/ui/widgets/postCardWidget.dart';
import 'package:insta_followers/src/services/storageManager.dart';
import 'package:provider/provider.dart';

class PostList extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return PostListState();
  }

}

class PostListState extends State<PostList> {

  Color bgColor = Color(0xff222831);
  static const Color darkColor = const Color(0xff151a30);

  String postUrl;
  String authorName;
  String thumbnailUrl;
  String winnerPost;
  String nativeAdUnitId;
  Timer timer;
  double _height = 250;
  List<Object> postList = List<Object>.empty();
  StreamController<List<Object>> _postListStreamController = StreamController();

  InterstitialAd interstitialPostList;
  NativeAd nativeAd;

  bool _isNativeAdLoaded = false;

  @override
  void initState() {
    super.initState();
    prepareList();
  }

  // Ads management section
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    final adState = Provider.of<AdState>(context);
    adState.initialization.then((status) {
      nativeAdUnitId = adState.nativePostListAdUnit;
      interstitialPostList = InterstitialAd(
          adUnitId: adState.interstitialPostAdUnitId,
          listener: AdListener(
              onAdLoaded: (_) {
                interstitialPostList.show();
              },
              onAdFailedToLoad: (_ ,err) {
                interstitialPostList?.dispose();
              },
              onAdClosed: (_) {
                interstitialPostList?.dispose();
              }
          ),
          request: AdRequest()
      );

      nativeAd = NativeAd(
        adUnitId: adState.nativePostListAdUnit,
        factoryId: 'listTile',
        request: AdRequest(),
        listener: AdListener(
          onAdLoaded: (_) {
            setState(() {
              _isNativeAdLoaded = true;
            });
          },
          onAdFailedToLoad: (_, error) {
            print('Ad load failed (code=${error.code} message=${error.message})');
          },
        ),
      )..load();
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      backgroundColor: bgColor,
      body: Padding(
        padding: const EdgeInsets.only(top: 10),
        child: RefreshIndicator(
          child:
          StreamBuilder(
            initialData: postList,
            stream: _postListStreamController.stream,
            builder: (context, AsyncSnapshot<List<Object>> snapshot){
              if(snapshot.connectionState == ConnectionState.waiting) {
                return Center(child:CircularProgressIndicator());
              }else if(snapshot.hasData) {
                return ListView.builder(
                      itemCount: snapshot.data.length,
                        itemBuilder: (context, index) {
                          return snapshot.data[index] is Post ?
                              PostCardWidget(
                                  (snapshot.data[index] as Post).thumbnailUrl ,
                                  (snapshot.data[index] as Post).authorName,
                                  (snapshot.data[index] as Post).postId,
                                  this
                              )
                          : _isNativeAdLoaded == true ?
                          Container(
                              height: _height,
                              margin: EdgeInsets.fromLTRB(15, 0, 15, 10),
                              padding: EdgeInsets.all(10),
                              decoration: BoxDecoration(
                                  color: Colors.purple,
                                  borderRadius: BorderRadius.only(topRight: Radius.circular(10), topLeft: Radius.circular(10)),
                                  border: Border.all(color: Colors.red, width: 2)
                              ),
                              child: AdWidget(ad: postList[index] as NativeAd,)
                          )
                          : Center();
                        }
                );
              }
              return CircularProgressIndicator();
            }
          ),
          onRefresh: _refreshList,
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: addPostDialog,
        child: Icon(Icons.add),
        backgroundColor: Colors.purple,
      ),
    );
  }

  @override
  void dispose() {
    interstitialPostList?.dispose();
    _postListStreamController.close();
    timer.cancel();
    nativeAd.dispose();
    super.dispose();
  }

  void addPostDialog() {
    showDialog(
      context: context,
      builder: (_) => Dialog(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(30)
        ),
        child: Material(
            child: Container(
              height: 200,
              padding: EdgeInsets.fromLTRB(8, 8, 8, 0),
              color: Colors.purple,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  // description
                  RichText(
                      text: TextSpan(
                        text: '• ',
                        style: TextStyle(color: Colors.deepOrange, fontSize: 15),
                        children: <TextSpan>[
                          TextSpan(
                            text:
                            "Remember private account can't share their post here.",
                            style: TextStyle(color: Colors.white, fontSize: 15, height: 1.2),
                          ),
                          TextSpan(text: '\n• ',
                              children: <TextSpan>[
                                TextSpan(text:
                                "Copy your post link from Instagram and past it here.",
                                  style: TextStyle(color: Colors.white, fontSize: 15, height: 1.2),
                                ),
                              ]
                          ),
                        ],
                      )),
                  SizedBox(height: 10,),
                  // post input text box
                  TextField(
                    decoration: InputDecoration(
                      filled: true,
                      fillColor: bgColor,
                      hintStyle: TextStyle(color: Colors.white),
                      hintText: "Past your post link"
                    ),
                    onChanged: (string) {
                      postUrl = string;
                    },
                    style: TextStyle(color: Colors.white),
                  ),
                  SizedBox(height: 5,),
                  // post submit button
                  ElevatedButton(
                      onPressed: () {
                        if(likedCount == 3) {
                          interstitialPostList.load();
                          postProcess(context);
                          Navigator.pop(context);
                          likedCount = 0;
                        }else {
                          Util().showSnackBar(context, "First like 3 post of others!");
                          Navigator.pop(context);
                        }
                      },
                      child: Text("Submit")
                  )
                ],
              ),
            )
        ),
      )
    );
  }

  void postProcess(context) {
    String _postID;
    try {
      _postID = postUrl.substring(postUrl.indexOf("p/"),postUrl.indexOf("/?")).split("/")[1];
    } catch(err) {
      Util().showSnackBar(context, "Invalid post link");
    }
    fetchPostDetail(_postID);
  }

  void fetchPostDetail(String postId) async{
    try {
      Response response = await PrepareJson().generatePostEmbedLink(postId, storageInstance.webUserAgent, storageInstance.webAppId);
      var result = jsonDecode(response.toString());
      storageInstance.saveUserPost(result['thumbnail_url'], postId, result['author_name']);
      postIDG = postId;
    } on DioError catch (err) {
      Util().showSnackBar(context, "Failed to get post. ${err.response.data}");
      print("Failed to get post. ${err.response.data}");
    }
  }

  Future<void> _refreshList() async {
    storageInstance.getUsersPost().then((value) {
      if(DateTime.fromMillisecondsSinceEpoch(value.data.first.createdat * 1000, isUtc: true).isAfter(DateTime.now().toUtc())) {
        winnerPost = value.data.first.postId;
      }else {
        winnerPost = "";
      }
      postList = List.from(value.data);
      for (int i = postList.length - 2; i >= 2; i -= 3) {
        postList.insert(
            i,
            nativeAd
        );
      }
      _postListStreamController.sink.add(postList);
    });
  }

  void updateState() {
    setState(() {});
  }

  prepareList() async {
    _refreshList();
    timer = Timer.periodic(Duration(seconds: 15), (Timer t) => _refreshList());
  }

}