
import 'dart:async';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:insta_followers/src/model/nodeModel.dart';
import 'package:insta_followers/src/services/ad_state.dart';
import 'package:insta_followers/src/services/globalVariable.dart';
import 'package:insta_followers/src/services/instagramAPI.dart';
import 'package:insta_followers/src/services/storageManager.dart';
import 'package:insta_followers/src/ui/screens/myApp.dart';
import 'package:insta_followers/src/ui/screens/terms.dart';
import 'package:insta_followers/src/services/util.dart';
import 'package:provider/provider.dart';

class FollowerInsight extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return FollowerInsightState();
  }

}

class FollowerInsightState extends State<FollowerInsight> {
  StorageManager storageInstance = StorageManager();
  int unfollowIndex;
  Edges response;
  Map followerMap = {};
  List diffList;
  bool _isDataLoaded = false;
  Timer _videoAdsWaitingTime;

  InterstitialAd interstitialUnfollowList;
  RewardedAd rewardedUnfollowList;

  _logOutProcess() async{
    storageInstance.saveDataPref('isLoggedIn', false);
    await PrepareJson().logOutRequest();
    differencesMap.clear();
    isLogin = false;
    // cookie = '';
    csrftoken = '';
    cookie = storageInstance.instaCookie;
    setState(() {});
  }

  _unFollowProcess(int index) async{
    setState(() {
      unfollowIndex = index;
    });
    try{
      Response response = await PrepareJson().unfollowRequest(diffList[index]['id']);
      if(response.statusCode == 200){
        differencesMap.remove(diffList[index]['id']);
        setState(() {
          unFollowCount += 1;
          unfollowIndex = null;
          _loadFetchedData();
        });
      }
    }catch (err){
      if(err.toString().split(':').last.trim() == 'You are not login!') {
        Util().showSnackBar(context, 'You are not login. try login again!');
        _logOutProcess();
      }
      else if(err.toString().split(':').last.trim() == 'Error happened. try again'){
        Util().showSnackBar(context, 'Error happened. try again!');
      }
    }
  }

  _fetchFollowerList() async{
    try{
      Response userNameResponse = await PrepareJson().checkLoginUsername();
      String _userName = userNameResponse.data['form_data']['username'];
      if(instaID != _userName) {
        _logOutProcess();
        Util().showSnackBar(context, 'Wrong username');
        return;
      }
    }catch(err){
      _logOutProcess();
      Util().showSnackBar(context, '$err');
    }
    //get follower list
    isLoadStart = true;
    do{
      response = await PrepareJson().getFollowerJson('follower', cookie, response== null? null: response.info.endCursor);
      for(var item in response.edges) {
          Map temp = {item.id:{'id':item.id,'username':item.userName,
            'fullName':item.fullName,'profilepic':item.profilePicUrl}};
          followerMap.addAll(temp);
      }
    }while(response.info.hasNextPage && followerMap.length < 500);
    //get following list
    do{
      response = await PrepareJson().getFollowerJson('following', cookie, response== null? null: response.info.endCursor);
      for(var item in response.edges) {
        if (!followerMap.containsKey(item.id)) {
          Map temp = {item.id: {'id': item.id, 'username': item.userName,
            'fullName':item.fullName,'profilepic': item.profilePicUrl}};
          differencesMap.addAll(temp);
        }
      }
    }while(response.info.hasNextPage && differencesMap.length < 50);
    diffList = differencesMap.values.toList();
    setState(() {
      isLoadStart = false;
      _isDataLoaded = true;
    });
  }

  _loadFetchedData() async{
    diffList = differencesMap.values.toList();
    setState(() {
      _isDataLoaded = true;
    });
  }

  @override
  void initState() {
    super.initState();
    if(isLogin && differencesMap.isEmpty && !isLoadStart && isItOkToShow){
      // PrepareJson().checkLoginUsername();
     _fetchFollowerList();
    }else if(differencesMap.isNotEmpty){
     _loadFetchedData();
    }
  }

  // Ads management
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    final adState = Provider.of<AdState>(context);
    adState.initialization.then((status) {
      rewardedUnfollowList = RewardedAd(
          adUnitId: adState.rewardedUnfollowAdUnit,
          listener: AdListener(
              onAdLoaded: (_) {
                Navigator.pop(context);
                _videoAdsWaitingTime?.cancel();
                rewardedUnfollowList.show();
              },
              // onAdFailedToLoad: (_ ,err) {
              //   Util().showSnackBar(context, "Failed to load video, Try again later.");
              //   Navigator.pop(context);
              //   _videoAdsWaitingTime?.cancel();
              //   rewardedUnfollowList?.dispose();
              // },
              onAdClosed: (_) {
                rewardedUnfollowList?.dispose();
              },
              onRewardedAdUserEarnedReward: (_ ,RewardItem item) {
                rewardedUnfollowList?.dispose();
                setState(() {
                  _fetchFollowerList();
                  isItOkToShow = true;
                  _isDataLoaded = false;
                });
              }
          ),
          request: AdRequest()
      );
      interstitialUnfollowList = InterstitialAd(
          adUnitId: adState.interstitialUnfollowAdUnitId,
          listener: AdListener(
            onAdLoaded: (_) {
              interstitialUnfollowList.show();
            },
            onAdFailedToLoad: (_ ,err) {
              interstitialUnfollowList?.dispose();
            },
            onAdClosed: (_) {
              interstitialUnfollowList?.dispose();
            }
          ),
          request: AdRequest()
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff222831),
      body:
      isLogin?
          //is login
          !isItOkToShow?
              //it is not ok to show the result
          Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  height: 50,
                  width: double.infinity,
                  margin: EdgeInsets.fromLTRB(15, 0, 15, 0),
                  child: ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all<Color>(Colors.green),
                      shape: MaterialStateProperty.all<OutlinedBorder>(
                          RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              side: BorderSide(color: Colors.red)
                          )
                      ),
                    ),
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => MyApp())
                      );
                    },
                    child: Text('Promote yourself once',
                      style: TextStyle(color: Colors.white),),
                  ),
                ),
                SizedBox(height: 5,),
                Center(
                  child: Text(
                    'OR',
                    style: TextStyle(color: Colors.white, fontSize: 20),
                  ),
                ),
                SizedBox(height: 5,),
                Container(
                  height: 50,
                  width: double.infinity,
                  margin: EdgeInsets.fromLTRB(15, 0, 15, 0),
                  child: ElevatedButton(
                    style: ButtonStyle(
                      backgroundColor: MaterialStateProperty.all<Color>(Colors.green),
                      shape: MaterialStateProperty.all<OutlinedBorder>(
                          RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                              side: BorderSide(color: Colors.red)
                          )
                      ),
                    ),
                    onPressed: () {
                      rewardedUnfollowList.load();
                      // Ads fail waiting time
                      _videoAdsWaitingTime = Timer(
                          Duration(seconds: 10),
                              () {
                            Navigator.pop(context);
                            Util().showSnackBar(context, "Failed to load video, Try again later.");
                            rewardedUnfollowList.dispose();
                          }
                      );
                      // Ads waiting dialog
                      showDialog(
                          context: context,
                          barrierDismissible: false,
                          builder: (_) => Dialog(
                        child: Container(
                          color: Colors.green,
                          padding: EdgeInsets.fromLTRB(20, 7, 7, 7),
                          height: 50,
                          child: Center(
                            child: Row(
                              children: [
                                CircularProgressIndicator(),
                                SizedBox(width: 30,),
                                Text("Waiting to load video...", style: TextStyle(color: Colors.white),)
                              ],
                            ),
                          ),
                        ),
                      ));
                    },
                    child: Text('Watch a video Ads',
                      style: TextStyle(color: Colors.white),),
                  ),
                ),
              ],
            ),
          )
              :
              // it is ok to show the result
      Container(
        child: Center(
          child: Flex(
            direction: Axis.vertical,
            children: [
              // waiting to fetch data
              !_isDataLoaded?
              Padding(
                padding: const EdgeInsets.only(top: 15),
                child: Column(
                  children: [
                    Center(
                      child: Text('Waiting to fetch data ...', style: TextStyle(color: Colors.white, fontSize: 15),),
                    ),
                    SizedBox(height: 20,),
                    CircularProgressIndicator(),
                  ],
                ),
              )
                  :
                  // data is ready to show
              Expanded(
                child: ListView.builder(
                  itemCount: diffList.length,
                  itemBuilder: (context, index){
                    return
                      ListTile(
                      leading: GestureDetector(
                        onTap: () {
                          showDialog(
                            context: context,
                            builder: (_) => Container(
                              decoration: new BoxDecoration(
                                  shape: BoxShape.circle,
                                  image: DecorationImage(
                                      fit: BoxFit.contain,
                                      image: NetworkImage(diffList[index]['profilepic'])
                                  )
                              ),
                            ),
                          );
                        },
                        child: Container(
                          height: 45,
                          width: 45,
                          decoration: new BoxDecoration(
                              shape: BoxShape.circle,
                              image: DecorationImage(
                                  image: NetworkImage(diffList[index]['profilepic'])
                              )
                          ),
                        ),
                      ),
                      title: Text(diffList[index]['fullName'], style: TextStyle(color: Colors.white),),
                      subtitle: Text(diffList[index]['username'], style: TextStyle(color: Colors.white),),
                      trailing: Container(
                        height: 30,
                        margin: EdgeInsets.fromLTRB(15, 0, 15, 0),
                        child: ElevatedButton(
                            style: ButtonStyle(
                              backgroundColor: MaterialStateProperty.all<Color>(Colors.white),
                              shape: MaterialStateProperty.all<OutlinedBorder>(
                                  RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10.0),
                                      side: BorderSide(color: Colors.red)
                                  )
                              ),
                            ),
                            // start to unfollow a person
                            onPressed: index == unfollowIndex? null
                              : unfollowIndex != null?
                                // there is a unfollow in process
                                () {
                                  Util().showSnackBar(context, 'Waiting another unfollow is in process...');
                                }
                                :
                                // there is no unfollow in process
                                () async{
                              if(unFollowCount < 5) {
                                await _unFollowProcess(index);
                              } else if(unFollowCount >= 5 && unFollowCount < 9) {
                                if(unFollowCount == 5 || unFollowCount == 8) {
                                  interstitialUnfollowList.load();
                                  _unFollowProcess(index);
                                }else {
                                  Util().showMyDialog(context);
                                  _unFollowProcess(index);
                                }
                              }else if(unFollowCount >= 9){
                                Util().showSnackBar(context, 'Too much unFollowed, try again later!');
                                return;
                              }
                            },
                            child: index != unfollowIndex?
                            Text('Unfollow',
                              style: TextStyle(color: Colors.black),)
                                :
                            Text('Processing...',
                              style: TextStyle(color: Colors.white),)
                        ),
                      ),
                    );
                  }
                ),
              )
            ],
          ),
        ),
      )
          :
          //is not login
      Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            child: Center(
              child: Container(
                height: 50,
                width: double.infinity,
                margin: EdgeInsets.fromLTRB(15, 0, 15, 0),
                child: ElevatedButton(
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all<Color>(Colors.green),
                    shape: MaterialStateProperty.all<OutlinedBorder>(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                          side: BorderSide(color: Colors.red)
                        )
                    ),
                  ),
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => Terms())
                    );
                  },
                  child: Text('Login with Instagram',
                    style: TextStyle(color: Colors.white),),
                ),
              ),
            ),
          ),
          SizedBox(height: 20,),
          Container(
            padding: EdgeInsets.fromLTRB(20, 50, 20, 20),
            child: RichText(
                text: TextSpan(
                  text: '• ',
                  style: TextStyle(color: Colors.deepOrange, fontSize: 15),
                  children: <TextSpan>[
                    TextSpan(
                      text:
                      'Be careful to login with the Instagram username that you entered before!',
                      style: TextStyle(color: Colors.white, fontSize: 15, height: 1.5),
                    ),
                    TextSpan(text: '\n• ',
                        children: <TextSpan>[
                          TextSpan(text:
                          "We only show the latest 50 person who followed recently by you and they don't follow back.",
                            style: TextStyle(color: Colors.white, fontSize: 15, height: 1.5),
                          ),
                        ]
                    ),
                  ],
                )),
          ),
        ],
      )
    );
  }

  @override
  void dispose() {
    super.dispose();
    rewardedUnfollowList?.dispose();
    interstitialUnfollowList?.dispose();
    _videoAdsWaitingTime?.cancel();
  }

}
