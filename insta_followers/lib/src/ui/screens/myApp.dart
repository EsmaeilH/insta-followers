
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:in_app_review/in_app_review.dart';
import 'package:insta_followers/src/ui/screens/followerInsight.dart';
import 'package:insta_followers/src/ui/screens/followerList.dart';
import 'package:insta_followers/src/ui/screens/lottery.dart';
import 'package:insta_followers/src/ui/screens/postList.dart';
import 'package:insta_followers/src/services/globalVariable.dart';

import 'about.dart';
import 'loginPage.dart';

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MyAppState();
  }
}

class MyAppState extends State<MyApp> {
  int selectedIndex = 0;
  String winnerUser;
  static const Color darkColor = const Color(0xff151a30);
  Color bgColor = Color(0xff222831);
  final TextStyle textStyle = TextStyle(color: Colors.white, fontSize: 18);
  final InAppReview inAppReview = InAppReview.instance;

  inAppReviewRequest() async {
    if (await inAppReview.isAvailable()) {
      inAppReview.requestReview();
    }
  }

  void _onItemTapped(int index) {
    setState(() {
      selectedIndex = index;
    });
  }

  void updateState() {
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: bgColor,
      appBar: selectedIndex == 0 ? null :PreferredSize(
          preferredSize: Size.fromHeight(120),
          child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(30),
                          bottomRight: Radius.circular(30)),
                      color: darkColor),
                  child: Container(
                    padding: const EdgeInsets.fromLTRB(15, 20, 15, 0),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            GestureDetector(
                              onTap: () {
                                FirebaseAnalytics().logEvent(
                                    name: 'loginPageBtn', parameters: null);
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => LoginPage()));
                              },
                              child: Container(
                                height: 40,
                                width: 40,
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle,
                                    image: DecorationImage(
                                      fit: BoxFit.fill,
                                      image: AssetImage('assets/Icon/Icon.png'),
                                    )),
                              ),
                            ),
                            Text(
                              '$instaID',
                              style:
                                  TextStyle(color: Colors.white, fontSize: 20),
                            ),
                            GestureDetector(
                              child: Container(
                                height: 30,
                                width: 30,
                                decoration: BoxDecoration(
                                    image: DecorationImage(
                                        image: AssetImage(
                                            'assets/Icon/question-mark.png'))),
                              ),
                              onTap: () {
                                FirebaseAnalytics().logEvent(
                                    name: 'questionBtn', parameters: null);
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => About()));
                              },
                            )
                          ],
                        ),
                        SizedBox(
                          height: 18,
                        ),
                        // appbar title text
                        Column(
                          children: [
                            selectedIndex == 1?
                            Text(
                              'Posts',
                              style:
                                  TextStyle(color: Colors.white, fontSize: 25),
                            )
                            : selectedIndex == 2?
                            Text(
                              'Unfollow',
                              style:
                              TextStyle(color: Colors.white, fontSize: 25),
                            )
                            :
                            Text(
                              'Lottery',
                              style:
                              TextStyle(color: Colors.white, fontSize: 25),
                            ),
                            SizedBox(
                              height: 21,
                            ),
                          ],
                        ),
                        SizedBox(
                          height: 10,
                        ),
                      ],
                    ),
                  ),
                )),
      body: selectedIndex == 0
          ?
          // Follower list page
          FollowerList()
          : selectedIndex == 1
          ?
          // post list page
          PostList()
          : selectedIndex == 2
          ?
          // Insight page
          FollowerInsight()
          :
          // Lottery page
          Lottery(this),
      // Bottom navigation
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: ImageIcon(AssetImage('assets/Icon/follow.png')),
            label: 'Follow',
            backgroundColor: darkColor,
          ),
          BottomNavigationBarItem(
            icon: ImageIcon(AssetImage('assets/Icon/like.png')),
            label: 'Post',
            backgroundColor: Colors.purple,
          ),
          BottomNavigationBarItem(
            icon: ImageIcon(AssetImage('assets/Icon/unfollow.png')),
            label: 'Unfollow',
            backgroundColor: Colors.green,
          ),
          BottomNavigationBarItem(
            icon: ImageIcon(AssetImage('assets/Icon/lottery.png')),
            label: 'Lottery',
            backgroundColor: Colors.pink,
          ),
        ],
        currentIndex: selectedIndex,
        selectedItemColor: Colors.white,
        unselectedItemColor: Colors.white,
        onTap: _onItemTapped,
      ),
    );
  }

}
